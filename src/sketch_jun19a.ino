#include <SPI.h>
#include <PubSubClient.h>
#include <Ethernet.h>
#include <Servo.h>
#include <ArduinoJson.h>
#include <stdio.h>

Servo pitchServo;
Servo yawServo;
Servo rollServo;

int pitch_pos = 0;
int yaw_pos = 0;
int roll_pos = 0;

StaticJsonBuffer<200> jsonBuffer;

int connected_pin = 5;

void callback(char* topic, byte* payload, unsigned int length) {

  JsonObject& root = jsonBuffer.parseObject(payload);

  if (!root.success())
  {
    Serial.println("parseObject() failed");
    return;
  }
  Serial.println("parseObject() succeed");

  pitch_pos = root["pitch"];
  yaw_pos = root["yaw"];
  roll_pos = root["roll"];

  Serial.println(pitch_pos);
  Serial.println(yaw_pos);
  Serial.println(roll_pos);

  jsonBuffer.clear();
}

byte mac[]    = {  0x90, 0xA2, 0xDA, 0x0F, 0x70, 0xEA };
IPAddress ip(192, 168, 1, 115);

EthernetClient ethClient;
PubSubClient client("ec2-18-233-230-164.compute-1.amazonaws.com", 1883, callback, ethClient);

void reconnect() {
  while (!client.connected())
  {
    if (ethClient.connected())
    {
      Serial.println("connected");
    }
    else
    {
      Serial.println("connection failed");
    }

    Serial.print("Attempting MQTT connection...");
    
    if (client.connect("ard-cli")) {
      Serial.println("connected");

      client.subscribe("mychannel");

      digitalWrite(connected_pin, HIGH);
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");

      // Flash LED and wait
      digitalWrite(connected_pin, LOW);
      delay(5000);
      digitalWrite(connected_pin, HIGH);
      delay(200);
      digitalWrite(connected_pin, LOW);
      delay(200);
      digitalWrite(connected_pin, HIGH);
      delay(200);
      digitalWrite(connected_pin, LOW);
      delay(200);
    }
  }
}

void setup()
{
  Serial.begin(57600);

  if (Ethernet.begin(mac) == 0)
  {
    Serial.println("Failed to configure Ethernet using DHCP");
  }
  
  Serial.print("IP address: ");
  for (byte thisByte = 0; thisByte < 4; thisByte++)
  {
    Serial.print(Ethernet.localIP()[thisByte], DEC);
    Serial.print(".");
  }
  Serial.println();

  pitchServo.attach(9);
  rollServo.attach(8);
  yawServo.attach(7);

  pitchServo.write(0);
  rollServo.write(0);
  yawServo.write(0);

  pinMode(connected_pin, OUTPUT);
}

void loop()
{

  if (!client.connected())
  {
    reconnect();
  }
  client.loop();

  pitchServo.write(pitch_pos);
  yawServo.write(yaw_pos);
  rollServo.write(roll_pos);
}